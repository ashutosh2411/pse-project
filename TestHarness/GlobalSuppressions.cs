[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "The try-catch block intends to capture failures and move to the next one.", Scope = "member", Target = "Masti.TestHarness.TestHarnessTool.#DiscoverAssembliesTests(System.Collections.Generic.IList`1<System.String>)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA2210:AssembliesShouldHaveValidStrongNames", Justification = "Suppressed with permission from higher management.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "The try-catch block intends to capture failures and move to the next one.", Scope = "member", Target = "Masti.TestHarness.TestHarnessTool.#ExecuteAssemblyTests(System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.String>>)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1014:MarkAssembliesWithClsCompliant", Justification = "Suppressed with permission from higher management.")]

// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.
//
// To add a suppression to this file, right-click the message in the 
// Code Analysis results, point to "Suppress Message", and click 
// "In Suppression File".
// You do not need to add suppressions to this file manually.