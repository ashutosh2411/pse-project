﻿//-----------------------------------------------------------------------
// <author> 
//    Anish M M (anishmathewdev@gmail.com)
// </author>
//
// <date> 
//     25th October, 2018
// </date>
// 
// <reviewer> 
//
// </reviewer>
// 
// <copyright file="FileLogger.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//    This class provides functionality for logging to file and mailing the log
//    to a pre-specified e-mail id.
// </summary>
//-----------------------------------------------------------------------

namespace Masti.QualityAssurance
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Net.Mail;
    using System.Text;
    using System.Threading.Tasks;
    using static CommandLineOptions;

    /// <summary>
    /// Logger for logging into files. 
    /// </summary>
    public class FileLogger : ILogger
    {
        /// <summary>
        /// Lock for Log to ensure thread-safety.
        /// </summary>
        private readonly object logLock = new object();

        /// <summary>
        /// Lock for the log file to ensure thread-safety.
        /// </summary>
        private readonly object fileLock = new object();

        /// <summary>
        /// Holds path to log file if log has been written.
        /// </summary>
        private string filePath = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="FileLogger" /> class. 
        /// Sets local variables to permit specific types of log messages.
        /// </summary>
        /// <param name="messageTypes">Permitted type of Log messages.</param>     
        public FileLogger(IList<LogMessageType> messageTypes)
        {
            if (messageTypes == null)
            {
                throw new ArgumentNullException("messageTypes");
            }

            // Check if Info message should be printed.
            if (messageTypes.Contains(LogMessageType.Info))
            {
                this.AllowInfo = true;
            }

            // Check if Error message should be printed.
            if (messageTypes.Contains(LogMessageType.Error))
            {
                this.AllowError = true;
            }

            // Check if Success message should be printed.
            if (messageTypes.Contains(LogMessageType.Success))
            {
                this.AllowSuccess = true;
            }

            // Check if Warning message should be printed.
            if (messageTypes.Contains(LogMessageType.Warning))
            {
                this.AllowWarning = true;
            }
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="FileLogger"/> class.
        /// </summary>
        ~FileLogger()
        {
            // Commit log to file before logger object is destroyed.
            this.WriteLog();
        }

        /// <summary>
        /// Gets or sets a value indicating whether Info is to be logged.
        /// </summary>
        public bool AllowInfo { get; set; } = false;

        /// <summary>
        /// Gets or sets a value indicating whether Warning is to be logged.
        /// </summary>
        public bool AllowWarning { get; set; } = false;

        /// <summary>
        /// Gets or sets a value indicating whether Error is to be logged.
        /// </summary>
        public bool AllowError { get; set; } = false;

        /// <summary>
        /// Gets or sets a value indicating whether Success is to be logged.
        /// </summary>
        public bool AllowSuccess { get; set; } = false;

        /// <summary>
        /// Gets or sets log messages till it is written to file.
        /// </summary>
        public StringBuilder Log { get; set; } = new StringBuilder(string.Format(
            CultureInfo.CurrentCulture,
            string.Concat(Environment.NewLine, "NEW SESSION", Environment.NewLine)));

        /// <summary>
        /// Log Error type message.
        /// </summary>
        /// <param name="message">The message to be logged. </param>
        public void LogError(string message)
        {
            if (this.AllowError)
            {
                lock (this.logLock)
                {
                    this.Log.AppendLine(AddDate("[Error] " + message));
                }
            }
        }

        /// <summary>
        /// Log Info type message.
        /// </summary>
        /// <param name="message">The message to be logged. </param>
        public void LogInfo(string message)
        {
            if (this.AllowInfo)
            {
                lock (this.logLock)
                {
                    this.Log.AppendLine(AddDate("[Info] " + message));
                }
            }
        }

        /// <summary>
        /// Log Success type message.
        /// </summary>
        /// <param name="message">The message to be logged. </param>
        public void LogSuccess(string message)
        {
            if (this.AllowSuccess)
            {
                lock (this.logLock)
                {
                    this.Log.AppendLine(AddDate("[Success] " + message));
                }
            }
        }

        /// <summary>
        /// Log Warning type message.
        /// </summary>
        /// <param name="message">The message to be logged. </param>
        public void LogWarning(string message)
        {
            if (this.AllowWarning)
            {
                lock (this.logLock)
                {
                    this.Log.AppendLine(AddDate("[Warning] " + message));
                }
            }
        }

        /// <summary>
        /// Mail the logs to a pre-specified e-mail id.
        /// </summary>
        public void MailLog()
        {
            var settings = Properties.Settings.Default;

            // Make sure that no two threads are accessing the log file simultaneously.
            lock (this.fileLock)
            {
                try
                {
                    using (SmtpClient smtpServer = new SmtpClient("smtp.gmail.com"))
                    {
                        using (MailMessage mail = new MailMessage(settings.fromAddress, settings.toAddress))
                        {
                            mail.Subject = string.Format(
                                CultureInfo.CurrentCulture,
                                "Log [{0}]",
                                DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz", CultureInfo.CurrentCulture));
                            
                            // Make sure that no other thread is accessing the log while we copy the content.
                            lock (this.logLock)
                            {
                                // Check if log has been committed to file previously.
                                if (this.filePath != null)
                                {
                                    // Extract logs from current session alone.
                                    string source = System.IO.File.ReadAllText(this.filePath);
                                    string[] separators = new string[] { Environment.NewLine + "NEW SESSION" + Environment.NewLine };
                                    string[] tokens = source.Split(separators, StringSplitOptions.None);
                                    string currentSessionCommittedLog = tokens[tokens.Length - 1];

                                    // Prepend committed logs to currently uncommitted log.
                                    mail.Body = currentSessionCommittedLog + this.Log.ToString();
                                }
                                else
                                {
                                    // Use only the current log.
                                    mail.Body = this.Log.ToString();
                                }
                            }                            

                            smtpServer.Port = 25;
                            smtpServer.Credentials = new System.Net.NetworkCredential(settings.fromAddress, settings.mastiPassword);
                            smtpServer.EnableSsl = true;

                            smtpServer.Send(mail);
                        }
                    }
                }
                catch (SmtpException e)
                {
                    MastiDiagnostics.LogError(string.Format(CultureInfo.CurrentCulture, "Mail not sent :  {0}", e.Message));
                }
                catch (InvalidOperationException e)
                {
                    MastiDiagnostics.LogError(string.Format(CultureInfo.CurrentCulture, "Mail not sent :{0}", e.Message));
                }
                catch (Exception e)
                {
                    MastiDiagnostics.LogError(string.Format(CultureInfo.CurrentCulture, "Mail not sent : {0}", e.Message));
                    throw;
                }
            }                                
        }

        /// <summary>
        /// Commit accumulated logs to file.
        /// </summary>
        public void WriteLog()
        {
            // Make sure that no two threads are accessing the log file simultaneously.
            lock (this.fileLock)
            {
                string fileName = string.Format(
                CultureInfo.CurrentCulture,
                "Log_{0}.txt",
                DateTime.Now.ToString("MM_dd_yyyy", CultureInfo.CurrentCulture));

                // Get path to directory in which this module resides.
                string appPath = System.AppContext.BaseDirectory;
                this.filePath = appPath + fileName;

                // Initialize stream writer for the log file. Append if file already exists.
                using (System.IO.StreamWriter logWriter = new System.IO.StreamWriter(this.filePath, true))
                {
                    // Lock the log so that noone modifies it while it is being written to file.
                    lock (this.logLock)
                    {
                        logWriter.Write(this.Log.ToString());
                        this.Log.Clear();
                    }                    
                }
            }            
        }

        /// <summary>
        /// Prepends current timestamp to the log message.
        /// </summary>
        /// <param name="message">Message to be logged.</param>
        /// <returns>Message prepended with timestamp.</returns>
        private static string AddDate(string message)
        {
            return DateTime.Now.ToString("HH:mm:ss.ff", CultureInfo.CurrentCulture) + " " + message;
        }
    }
}