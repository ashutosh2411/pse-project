﻿//-----------------------------------------------------------------------
// <author> 
//    Gautam Kumar
// </author>
//
// <date> 
//     11-oct-2018 
// </date>
// 
// <reviewer> 
//     Aryan Raj
// </reviewer>
// 
// <copyright file="NetworkingStub.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//      The following file contain our SchemaStub.
// </summary>
//-----------------------------------------------------------------------

namespace Masti.Messenger.Stubs
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using Masti.Networking;

    /// <summary>
    /// NetworkingStub for implementing Icommunication 
    /// </summary>
    public class NetworkingStub : ICommunication
    {
        /// <summary>
        /// local IP
        /// </summary>
        public string LocalIP => "127.0.0.1";

        /// <summary>
        /// Connected flag
        /// </summary>
        public bool Connected => throw new NotImplementedException();

        /// <summary>
        /// Dummy send function
        /// </summary>
        /// <param name="msg">Dummy message</param>
        /// <param name="targetIP">target IP</param>
        /// <param name="type">type of the data</param>
        /// <returns>return the success or failure of send function</returns>
        public bool Send(string msg, IPAddress targetIP, DataType type)
        {
            return true;
        }

        /// <summary>
        /// stop communication
        /// </summary>
        public void StopCommunication()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Dummy SubscribeForDataReceival
        /// </summary>
        /// <param name="type">type of the data</param>
        /// <param name="receivalHandler">recieve handler</param>
        /// <returns>return the success or failuer of subscribe</returns>
        public bool SubscribeForDataReceival(DataType type, DataReceivalHandler receivalHandler)
        {
            return true;
        }

        /// <summary>
        /// Dummy SubscribeForDataStatus
        /// </summary>
        /// <param name="type">type of the data</param>
        /// <param name="statusHandler">status handler</param>
        /// <returns>return the success or failuer of subscribe</returns>
        public bool SubscribeForDataStatus(DataType type, DataStatusHandler statusHandler)
        {
            return true;
        }
    }
}
