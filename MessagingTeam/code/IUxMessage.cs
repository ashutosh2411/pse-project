//-----------------------------------------------------------------------
// <author> 
//     Aryan Raj 
// </author>
//
// <date> 
//     12-10-2018 
// </date>
// 
// <reviewer> 
//     Aryan Raj 
// </reviewer>
// 
// <copyright file="IUxMessage.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//      This file implements interfaces used by UX team .
// </summary>
//-----------------------------------------------------------------------

namespace Masti.Messenger
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Net;
    using static Masti.Messenger.Handler;

    /// <summary>
    /// This interface is used by UX to send, receive, delete and retrieve messages
    /// </summary>
    public interface IUXMessage
    {
        /// <summary>
        /// This is used by UX module to send message and IP and port
        /// </summary>
        /// <param name="message">text message</param>
        /// <param name="toIP"> IP where message is to be sent</param>
        /// <param name="dateTime">date and  time at which message was sent</param>
        /// <returns>true or false</returns>
        bool SendMessage(string message, string toIP, string dateTime);

        /// <summary>
        /// This is subscriber function which will be used by UX module to subscribe their callbacks for receiving data
        /// </summary>
        /// <param name="handler">delegates to run callbacks</param>
        /// <returns>true or false</returns>
        bool SubscribeToDataReceiver(DataReceiverHandler handler);

        /// <summary>
        /// This is subscriber function which will be used by UX module to subscribe their callbacks for getting status
        /// </summary>
        /// <param name="handler">delegates to run callbacks</param>
        /// <returns>true or false</returns>
        bool SubscribeToStatusReceiver(DataStatusHandlers handler);

        /// <summary>
        /// This is interface to retrieve message from database
        /// </summary>
        /// <param name="startSession">start session</param>
        /// <param name="endSession">end session</param>
        /// <returns>packed string stored in database</returns>
        string RetrieveMessage(int startSession, int endSession);

        /// <summary>
        /// This is interface to delete message of database for a given time interval
        /// </summary>
        /// <param name="startSession">start session</param>
        /// <param name="endSession">end session</param>
        /// <returns>1 or 0</returns>
        int DeleteMessage(int startSession, int endSession);

        /// <summary>
        /// This is for storing message  in database
        /// </summary>
        /// <param name="currSession">dictionary to store message of a particular session</param>
        /// <returns>true or false</returns>
        bool StoreMessage(Dictionary<string, string> currSession);

        /// <summary>
        /// This function is used to establish the connection by sending the first messag
        /// </summary>
        /// <param name="userName">user name</param>
        /// <param name="toIP">target IP</param>
        /// <returns>true or false</returns>
        bool Connectify(string userName, string toIP);

        /// <summary>
        /// This function is to subscribe callbacks of UX model for starting connection by sending the first message
        /// </summary>
        /// <param name="handler">call back function</param>
        /// <returns>true or false</returns>
        bool SubscribeToConnectifier(ConnectHandlers handler);

        /// <summary>
        /// Returns the current session ID.
        /// </summary>
        /// <returns>Current session ID</returns>
        /// <returns>1 or 0</returns>
        int GetCurrentSessionId();
    }
}