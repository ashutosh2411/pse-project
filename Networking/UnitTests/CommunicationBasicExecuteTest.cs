﻿// -----------------------------------------------------------------------
// <author> 
//      Rohith Reddy G
// </author>
//
// <date> 
//      28/10/2018
// </date>
// 
// <reviewer>
//      Libin, Parth, Jude
// </reviewer>
//
// <copyright file="CommunicationBasicExecuteTest.cs" company="B'15, IIT Palakkad">
//      This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
//
// <summary>
//      This file contains the test case for a basic functionality all positive
// </summary>
// -----------------------------------------------------------------------

namespace Masti.Networking.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading;
    using Masti.QualityAssurance;
    using Masti.Schema;

    /// <summary>
    /// This class executes a basic test which is all positive
    /// </summary>
    public class CommunicationBasicExecuteTest : ITest
    {
        /// <summary>
        /// event to pause the thread from executing
        /// this event is set by ProfDataReceival
        /// </summary>
        private static ManualResetEvent manualResetProfData = new ManualResetEvent(false);

        /// <summary>
        /// event to pause the thread from executing
        /// this event is set by StudDataStatus
        /// </summary>
        private static ManualResetEvent manualResetProfStatus = new ManualResetEvent(false);

        /// <summary>
        /// event to pause the thread from executing
        /// this event is set by StudDataReceival
        /// </summary>
        private static ManualResetEvent manualResetStudData = new ManualResetEvent(false);

        /// <summary>
        /// event to pause the thread from executing
        /// this event is set by StudDataStatus
        /// </summary>
        private static ManualResetEvent manualResetStudStatus = new ManualResetEvent(false);

        /// <summary>
        /// schema module instance
        /// </summary>
        private ISchema schema = new MessageSchema();

        /// <summary>
        /// pointer to check the data received
        /// </summary>
        private bool profData = false;

        /// <summary>
        /// pointer to check the data status
        /// </summary>
        private bool profStatus = false;

        /// <summary>
        /// pointer to check the data received
        /// </summary>
        private bool studData = false;

        /// <summary>
        /// pointer to check the data status
        /// </summary>
        private bool studStatus = false;

        /// <summary>
        /// message to be sent 
        /// </summary>
        private string message = "hi";

        /// <summary>
        /// ip address of the machine to which data is sent 
        /// </summary>
        private IPAddress ip = IPAddress.Parse("169.254.25.29");

        /// <summary>
        /// Initializes a new instance of the <see cref="CommunicationBasicExecuteTest" /> class.
        /// </summary>
        /// <param name="logger">Logger object used for logging during test</param>
        public CommunicationBasicExecuteTest(ILogger logger)
        {
            this.Logger = logger;
        }

        /// <summary>
        /// Gets or sets Logger instance.
        /// </summary>
        public ILogger Logger { get; set; }

        /// <summary>
        /// This function is the part of ITest interface from where the execution starts 
        /// </summary>
        /// <returns>true or false</returns>
        public bool Run()
        {
            using (Communication profCommunication = new Communication(8100, 3, 1))
            {
                using (Communication studentCommunication = new Communication(IPAddress.Parse(profCommunication.LocalIP), 8100, 3, 1))
                {
                    this.ip = IPAddress.Parse(profCommunication.LocalIP);
                    /* subscribe for data status*/
                    studentCommunication.SubscribeForDataStatus(DataType.Message, this.StudDataStatus);

                    /* subscribe for data receival*/
                    studentCommunication.SubscribeForDataReceival(DataType.Message, this.StudDataReceival);

                    /* subscribe for data receival */
                    profCommunication.SubscribeForDataReceival(DataType.Message, this.ProfDataReceival);

                    /* subscribe for data status */
                    profCommunication.SubscribeForDataStatus(DataType.Message, this.ProfDataStatus);

                    /* message dict */
                    Dictionary<string, string> messageDict = new Dictionary<string, string>();

                    /* message added to dict */
                    messageDict.Add("message", this.message);

                    /* packet to be sent */
                    string packet = this.schema.Encode(messageDict);

                    /* send function */
                    studentCommunication.Send(packet, this.ip, DataType.Message);

                    manualResetProfStatus.WaitOne();
                    manualResetProfData.WaitOne();

                    /* send function */
                    profCommunication.Send(packet, this.ip, DataType.Message);

                    manualResetStudStatus.WaitOne();
                    manualResetStudData.WaitOne();
                }
            }

            if (this.profData && this.profStatus && this.studData && this.studStatus)
            {
                return true;
            }   
            else
            {
                return false;
            }
        }

        /// <summary>
        /// This function is used to check the status of the message which is sent 
        /// </summary>
        /// <param name="mess">message that is sent</param>
        /// <param name="status">status of the message</param>
        private void StudDataStatus(string mess, StatusCode status)
        {
            IDictionary<string, string> messageDict = this.schema.Decode(mess, false);

            if (messageDict["message"] == this.message && status == StatusCode.Success)
            {
                this.profStatus = true;
            }

            manualResetProfStatus.Set();
        }

        /// <summary>
        /// This function is called when a message is received
        /// We can extract the ip and data from this function 
        /// </summary>
        /// <param name="data">message data</param>
        /// <param name="fromIP">ip of the sender</param>
        private void ProfDataReceival(string data, IPAddress fromIP)
        {
            IDictionary<string, string> messageDict = this.schema.Decode(data, false);
            if (messageDict["message"] == this.message)
            {
                this.profData = true;
            }

            manualResetProfData.Set();
        }

        /// <summary>
        /// This function is used to check the status of the message which is sent 
        /// </summary>
        /// <param name="mess">message that is sent</param>
        /// <param name="status">status of the message</param>
        private void ProfDataStatus(string mess, StatusCode status)
        {
            IDictionary<string, string> messageDict = this.schema.Decode(mess, false);

            if (messageDict["message"] == this.message && status == StatusCode.Success)
            {
                this.studStatus = true;
            }

            manualResetStudStatus.Set();
        }

        /// <summary>
        /// This function is called when a message is received
        /// We can extract the ip and data from this function 
        /// </summary>
        /// <param name="data">message data</param>
        /// <param name="fromIP">ip of the sender</param>
        private void StudDataReceival(string data, IPAddress fromIP)
        {
            IDictionary<string, string> messageDict = this.schema.Decode(data, false);
            if (messageDict["message"] == this.message)
            {
                this.studData = true;
            }

            manualResetStudData.Set();
        }
    }
}
